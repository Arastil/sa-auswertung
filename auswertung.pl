#!/usr/bin/env perl

use strict;
use warnings;

my $threshold = 0.5;
my $now_for_real = 0;

my $interval_border_time = 0;
my $interval_counter = 0;
my $interval_sum = 0;

my $state;

my @on_intervals = ();
my @on_counter = ();
my @on_sum = ();
my @off_intervals = ();
my @off_counter = ();

while (my $line = <STDIN>) {
  $line =~ s/,/./g;
  my ($time, $value) = split('\t', $line);

  my $new_state = 1;
  $new_state = 0 if $value < $threshold;

  $interval_sum += $value;
  $interval_counter += 1;

  my $interval_length = 0;
  my $interval_median = 0;
  my $change = 0;
  if ( defined( $state ) && $new_state != $state ) {
    $change = 1;
    if ( $now_for_real ) {
      $interval_length = $time - $interval_border_time;
    }
    $now_for_real = 1;
    if ( $interval_length > 0 ) {
      if ( $new_state ) {
        push @off_intervals, $interval_length;
        push @off_counter, $interval_counter;
      }
      else {
        push @on_intervals, $interval_length;
        push @on_counter, $interval_counter;
        push @on_sum, $interval_sum;
      }
    } 
    $interval_border_time = $time;
    $interval_sum = 0;
    $interval_counter = 0;
  }
  $state = $new_state;

  print "time: $time - value: $value - state: $state - change: $change - interval: $interval_length\n";
}

print "=======> on:\n";
print join( "\n", @on_intervals );
print "\n";
print "=======> on counter:\n";
print join( "\n", @on_counter );
print "\n";
print "=======> off:\n";
print join( "\n", @off_intervals );
print "\n";
print "=======> off counter:\n";
print join( "\n", @off_counter );
print "\n";
print "=======> sum:\n";
print join( "\n", @on_sum );
print "\n";

my $n = 0;
my $sum = 0;
my $counter = 0;
for my $set ( @on_intervals ) {
  if ( $on_counter[ $n ] && $off_counter[ $n ] ) {
    my $median = $on_sum[ $n ] / ( $on_counter[ $n ] + $off_counter[ $n ] );
    $sum += $median;
    $counter += 1;
    print "$n: " . $on_intervals[ $n ] . " + " . $off_intervals[ $n ] . " => " . $median . "\n";
  }
  $n += 1;
}
my $end_result = $sum / $counter;
print "end result: $end_result\n";
